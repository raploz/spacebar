# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the spacebar package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: spacebar\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-16 00:51+0000\n"
"PO-Revision-Date: 2023-05-21 14:53+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.03.70\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Steve Allewell"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "steve.allewell@gmail.com"

#: daemon/channellogger.cpp:526 src/contents/ui/MessagesPage.qml:58
#, kde-format
msgid "New message"
msgstr "New message"

#: daemon/channellogger.cpp:533
#, kde-format
msgid "Message from %1"
msgstr "Message from %1"

#: daemon/channellogger.cpp:544
#, kde-format
msgctxt "Number of files attached"
msgid "%1 Attachment"
msgid_plural "%1 Attachments"
msgstr[0] "%1 Attachment"
msgstr[1] "%1 Attachments"

#: daemon/channellogger.cpp:562
#, kde-format
msgctxt "@action open message in application"
msgid "Open"
msgstr "Open"

#: daemon/main.cpp:29
#, kde-format
msgid "Spacebar background service"
msgstr "Spacebar background service"

#: src/chatlistmodel.cpp:95 src/contents/ui/MessagesPage.qml:49
#, kde-format
msgid "and %1 more"
msgstr "and %1 more"

#: src/contents/ui/ChatDetailPage.qml:14 src/contents/ui/MessagesPage.qml:131
#, kde-format
msgid "Details"
msgstr "Details"

#: src/contents/ui/ChatDetailPage.qml:30
#, kde-format
msgid "%1 person"
msgid_plural "%1 people"
msgstr[0] "%1 person"
msgstr[1] "%1 people"

#: src/contents/ui/ChatDetailPage.qml:52
#, kde-format
msgid "Add people"
msgstr "Add people"

#: src/contents/ui/ChatsPage.qml:17
#, kde-format
msgid "Chats"
msgstr "Chats"

#: src/contents/ui/ChatsPage.qml:43
#, kde-format
msgid "New Conversation"
msgstr "New Conversation"

#: src/contents/ui/ChatsPage.qml:52
#, kde-format
msgctxt "Configuring application settings"
msgid "Settings"
msgstr "Settings"

#: src/contents/ui/ChatsPage.qml:60
#, kde-format
msgctxt "Deleting a conversation"
msgid "Delete"
msgstr "Delete"

#: src/contents/ui/ChatsPage.qml:90
#, kde-format
msgctxt "Selecting recipients from contacts list"
msgid "Create a chat"
msgstr "Create a chat"

#: src/contents/ui/ChatsPage.qml:181
#, kde-format
msgctxt "Indicating that message was sent by you"
msgid "You"
msgstr "You"

#: src/contents/ui/ChatsPage.qml:181
#, kde-format
msgctxt "Indicating that message contains an image"
msgid "Picture"
msgstr "Picture"

#: src/contents/ui/ChatsPage.qml:270
#, kde-format
msgid "Delete this conversation?"
msgid_plural "Delete %1 conversations?"
msgstr[0] "Delete this conversation?"
msgstr[1] "Delete %1 conversations?"

#: src/contents/ui/ChatsPage.qml:271
#, kde-format
msgid "This is permanent and can't be undone"
msgstr "This is permanent and cannot be undone"

#: src/contents/ui/ContactsList.qml:45
#, kde-format
msgid "Select a number"
msgstr "Select a number"

#: src/contents/ui/ContactsList.qml:171
#, kde-format
msgctxt "Number of items selected"
msgid "%1 Selected"
msgstr "%1 Selected"

#: src/contents/ui/ContactsList.qml:181
#, kde-format
msgctxt "Open chat conversation window"
msgid "Next"
msgstr "Next"

#: src/contents/ui/ContactsList.qml:204
#, kde-format
msgid "Search or enter number…"
msgstr "Search or enter number…"

#: src/contents/ui/ContactsList.qml:312
#, kde-format
msgid "No contacts with phone numbers yet"
msgstr "No contacts with phone numbers yet"

#: src/contents/ui/ContactsList.qml:315
#, kde-format
msgid "Open contacts app"
msgstr "Open contacts app"

#: src/contents/ui/ContactsList.qml:322
#, kde-format
msgid "No results found"
msgstr "No results found"

#: src/contents/ui/MessagesPage.qml:125
#, kde-format
msgid "Call"
msgstr "Call"

#: src/contents/ui/MessagesPage.qml:144
#, kde-format
msgid "Texting this premium SMS number might cause you to be charged money"
msgstr "Texting this premium SMS number might cause you to be charged money"

#: src/contents/ui/MessagesPage.qml:155
#, kde-format
msgid "Max attachment limit exceeded"
msgstr "Max attachment limit exceeded"

#: src/contents/ui/MessagesPage.qml:166
#, kde-format
msgid "No MMSC configured"
msgstr "No MMSC configured"

#: src/contents/ui/MessagesPage.qml:177
#, kde-format
msgid "Message has expired and will be deleted"
msgstr "Message has expired and will be deleted"

#: src/contents/ui/MessagesPage.qml:188
#, kde-format
msgid "Message will be sent as individual messages"
msgstr "Message will be sent as individual messages"

#: src/contents/ui/MessagesPage.qml:463
#, kde-format
msgid "MMS message"
msgstr "MMS message"

#: src/contents/ui/MessagesPage.qml:471
#, kde-format
msgid "Message size: %1"
msgstr "Message size: %1"

#: src/contents/ui/MessagesPage.qml:477
#, kde-format
msgid "Expires: %1"
msgstr "Expires: %1"

#: src/contents/ui/MessagesPage.qml:593
#, kde-format
msgid "View all"
msgstr "View all"

#: src/contents/ui/MessagesPage.qml:733
#, kde-format
msgid "%1 new message"
msgid_plural "%1 new messages"
msgstr[0] "%1 new message"
msgstr[1] "%1 new messages"

#: src/contents/ui/MessagesPage.qml:811
#, kde-format
msgid "Copy code"
msgstr "Copy code"

#: src/contents/ui/MessagesPage.qml:820
#, kde-format
msgid "Copy link"
msgstr "Copy link"

#: src/contents/ui/MessagesPage.qml:832
#, kde-format
msgid "Copy text"
msgstr "Copy text"

#: src/contents/ui/MessagesPage.qml:841 src/contents/ui/MessagesPage.qml:893
#, kde-format
msgid "Save attachment"
msgstr "Save attachment"

#: src/contents/ui/MessagesPage.qml:852
#, kde-format
msgid "View slideshow"
msgstr "View slideshow"

#: src/contents/ui/MessagesPage.qml:865
#, kde-format
msgid "Delete message"
msgstr "Delete message"

#: src/contents/ui/MessagesPage.qml:875
#, kde-format
msgctxt "Retry sending message"
msgid "Resend"
msgstr "Resend"

#: src/contents/ui/MessagesPage.qml:924
#, kde-format
msgid "Save"
msgstr "Save"

#: src/contents/ui/MessagesPage.qml:931
#, kde-format
msgid "Cancel"
msgstr "Cancel"

#: src/contents/ui/MessagesPage.qml:1049
#, kde-format
msgctxt "Remove item from list"
msgid "Remove"
msgstr "Remove"

#: src/contents/ui/MessagesPage.qml:1087
#, kde-format
msgid "Write Message..."
msgstr "Write Message..."

#: src/contents/ui/MessagesPage.qml:1089
#, kde-format
msgctxt "%1 is a phone number"
msgid "Send Message from %1..."
msgstr "Send Message from %1..."

#: src/contents/ui/MessagesPage.qml:1142
#, kde-format
msgid "Choose a file"
msgstr "Choose a file"

#: src/contents/ui/NewConversationPage.qml:14
#, kde-format
msgid "Contacts"
msgstr "Contacts"

#: src/contents/ui/PreviewPage.qml:32
#, kde-format
msgid "Back"
msgstr "Back"

#: src/contents/ui/settings/MMSSettingsPage.qml:16
#, kde-format
msgid "MMS Settings"
msgstr "MMS Settings"

#: src/contents/ui/settings/MMSSettingsPage.qml:36
#, kde-format
msgid "Multimedia messages (MMS)"
msgstr "Multimedia messages (MMS)"

#: src/contents/ui/settings/MMSSettingsPage.qml:43
#, kde-format
msgid "MMSC"
msgstr "MMSC"

#: src/contents/ui/settings/MMSSettingsPage.qml:53
#, kde-format
msgid "Proxy"
msgstr "Proxy"

#: src/contents/ui/settings/MMSSettingsPage.qml:61
#, kde-format
msgid "Port"
msgstr "Port"

#: src/contents/ui/settings/MMSSettingsPage.qml:79
#, kde-format
msgid "Request delivery reports"
msgstr "Request delivery reports"

#: src/contents/ui/settings/MMSSettingsPage.qml:86
#, kde-format
msgid "Request read reports"
msgstr "Request read reports"

#: src/contents/ui/settings/MMSSettingsPage.qml:93
#, kde-format
msgid "Share delivery status"
msgstr "Share delivery status"

#: src/contents/ui/settings/MMSSettingsPage.qml:100
#, kde-format
msgid "Share read status"
msgstr "Share read status"

#: src/contents/ui/settings/MMSSettingsPage.qml:108
#, kde-format
msgid "Auto download messages"
msgstr "Auto download messages"

#: src/contents/ui/settings/MMSSettingsPage.qml:116
#, kde-format
msgid "Auto download messages for existing contacts only"
msgstr "Auto download messages for existing contacts only"

#: src/contents/ui/settings/MMSSettingsPage.qml:123
#, kde-format
msgid "Max message size (KiB)"
msgstr "Max message size (KiB)"

#: src/contents/ui/settings/MMSSettingsPage.qml:135
#, kde-format
msgid "Max attachments"
msgstr "Max attachments"

#: src/contents/ui/settings/MMSSettingsPage.qml:147
#, kde-format
msgid "Auto create SMIL"
msgstr "Auto create SMIL"

#: src/contents/ui/settings/MMSSettingsPage.qml:154
#, kde-format
msgid "Default to group conversations"
msgstr "Default to group conversations"

#: src/contents/ui/settings/SettingsPage.qml:16
#, kde-format
msgid "Settings"
msgstr "Settings"

#: src/contents/ui/settings/SettingsPage.qml:40
#, kde-format
msgid "General"
msgstr "General"

#: src/contents/ui/settings/SettingsPage.qml:45
#, kde-format
msgid "About"
msgstr "About"

#: src/contents/ui/settings/SettingsPage.qml:53
#, kde-format
msgid "Multimedia Messages (MMS)"
msgstr "Multimedia Messages (MMS)"

#: src/contents/ui/settings/SettingsPage.qml:61
#, kde-format
msgid "Restore defaults"
msgstr "Restore defaults"

#: src/contents/ui/settings/SettingsPage.qml:64
#, kde-format
msgid "Reset"
msgstr "Reset"

#: src/contents/ui/settings/SettingsPage.qml:85
#, kde-format
msgid "Appearance"
msgstr "Appearance"

#: src/contents/ui/settings/SettingsPage.qml:91
#, kde-format
msgid "Use custom colors for messages"
msgstr "Use custom colours for messages"

#: src/contents/ui/settings/SettingsPage.qml:99
#, kde-format
msgid "Incoming message color"
msgstr "Incoming message colour"

#: src/contents/ui/settings/SettingsPage.qml:122
#, kde-format
msgid "Outgoing message color"
msgstr "Outgoing message colour"

#: src/contents/ui/settings/SettingsPage.qml:145
#, kde-format
msgid "Message font size"
msgstr "Message font size"

#: src/contents/ui/settings/SettingsPage.qml:165
#, kde-format
msgid "Notifications"
msgstr "Notifications"

#: src/contents/ui/settings/SettingsPage.qml:171
#, kde-format
msgid "Show sender name / number"
msgstr "Show sender name / number"

#: src/contents/ui/settings/SettingsPage.qml:178
#, kde-format
msgid "Show a preview of the message content"
msgstr "Show a preview of the message content"

#: src/contents/ui/settings/SettingsPage.qml:185
#, kde-format
msgid "Show attachment previews"
msgstr "Show attachment previews"

#: src/contents/ui/settings/SettingsPage.qml:192
#, kde-format
msgid "Ignore tapbacks"
msgstr "Ignore tapbacks"

#: src/contents/ui/settings/SettingsPage.qml:209
#, kde-format
msgid "Choose a color"
msgstr "Choose a colour"

#: src/main.cpp:58
#, kde-format
msgid "SMS/MMS messaging client"
msgstr "SMS/MMS messaging client"

#: src/main.cpp:60
#, kde-format
msgid "© 2020-2021 KDE Community"
msgstr "© 2020-2021 KDE Community"

#: src/main.cpp:61
#, kde-format
msgid "Bhushan Shah"
msgstr "Bhushan Shah"

#: src/main.cpp:62
#, kde-format
msgid "Jonah Brüchert"
msgstr "Jonah Brüchert"

#: src/main.cpp:63
#, kde-format
msgid "Martin Klapetek"
msgstr "Martin Klapetek"

#: src/main.cpp:64
#, kde-format
msgid "Michael Lang"
msgstr "Michael Lang"

#: src/main.cpp:65
#, kde-format
msgid "Nicolas Fella"
msgstr "Nicolas Fella"

#: src/main.cpp:66
#, kde-format
msgid "Smitty van Bodegom"
msgstr "Smitty van Bodegom"

#: src/main.cpp:72
#, kde-format
msgid "Spacebar SMS/MMS client"
msgstr "Spacebar SMS/MMS client"

#: src/main.cpp:73
#, kde-format
msgid "Open a chat with the given phone number"
msgstr "Open a chat with the given phone number"

#~ msgid "Incoming"
#~ msgstr "Incoming"

#~ msgid "Outgoing"
#~ msgstr "Outgoing"

#~ msgid "Existing contacts only"
#~ msgstr "Existing contacts only"

#~ msgid "Other"
#~ msgstr "Other"

#~ msgid "Set"
#~ msgstr "Set"

#~ msgctxt "Invalid phone number"
#~ msgid "Unknown"
#~ msgstr "Unknown"

#~ msgctxt "Open chat conversation window"
#~ msgid "Compose"
#~ msgstr "Compose"

#~ msgid "Add/remove"
#~ msgstr "Add/remove"

#~ msgid "Duplicate recipient"
#~ msgstr "Duplicate recipient"

#~ msgid "Attachments"
#~ msgstr "Attachments"

#~ msgid "conversations?"
#~ msgstr "conversations?"

#~ msgid "Recipient"
#~ msgstr "Recipient"

#~ msgid "No chats yet"
#~ msgstr "No chats yet"

#~ msgid "Defaults"
#~ msgstr "Defaults"

#~ msgid "Send"
#~ msgstr "Send"

#~ msgid "The modem interface is not available"
#~ msgstr "The modem interface is not available"

#~ msgid ""
#~ "Could not find a sim account, can't open chat. Please check the log for "
#~ "details"
#~ msgstr ""
#~ "Could not find a sim account, cannot open chat. Please check the log for "
#~ "details"

#~ msgid "Failed to open chat: %1"
#~ msgstr "Failed to open chat: %1"

#~ msgid "Loading..."
#~ msgstr "Loading..."

#~ msgid "spacebar"
#~ msgstr "spacebar"
